Feature: Amazon search

  @search
  Scenario Outline: Searching books on amazon and result should be a list of books more than 10
    Given Amazon Web Page is Open
    When I search for keyword <SearchkeywordName>
    Then Result should be a new Page with title <titleName>
    And List of <resultName> more than 10

    Examples:
      | SearchkeywordName | titleName                    | resultName       |
      | "java books"      | "Amazon.com : java books"    | "books"          |
      | "iphone 11"       | "Amazon.com : iphone 11"     | "iphones"        |
      | "cucumber book"   | "Amazon.com : cucumber book" | "cucumber books" |
      | "keyboard"        | "Amazon.com : keyboard"      | "keyboards"      |
      | "tennis ball"     | "Amazon.com : tennis ball"   | "tennis balls"   |
