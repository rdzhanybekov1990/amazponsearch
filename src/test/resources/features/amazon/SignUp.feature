Feature: Sign UP

  Background:
    Given Amazon Web Page is Open
    When I click on Sign in button
    And I click on Create your Amazon Account

    @signOut
    Scenario: As a user when I try to sing up and use two different passwords
              it should reject and send a message
      Given Sign up page is open
      And I have first password filled as "testPassword"
      And I have confirm password filled as "testPassword2"
      When I send the form
      Then "Password must match" should be displayed.

