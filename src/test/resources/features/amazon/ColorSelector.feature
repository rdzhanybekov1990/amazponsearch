Feature: Color Selector on Product Page

  @colorSelector
  Scenario: As a user I want to be able to change the colors of the product
    Given I am on product page
    When I change a color of product
    Then the name of the color title should change