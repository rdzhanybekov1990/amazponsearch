Feature: DataTable

  Background:
    Given number
      | 10 |
      | 11 |
      | 23 |
      | 99 |

  @test
  Scenario: Testing Data Table
    When I divide it by 2
    Then I should get a sum of the results

  @test
  Scenario: Testing Data Table
    When I divide it by 3
    Then I should get a sum of the results

  @test
  Scenario: Testing Data Table
    When I divide it by 4
    Then I should get a sum of the results

  @test
  Scenario: Testing Data Table
    When I divide it by 5
    Then I should get a sum of the results
