Feature: Login

  Scenario: user should  able to login using his email and password
    Given login Page is opened
    And user's email "tomsmith"
    And user's password "SuperSecretPassword!"
    When I click on login button
    Then The new Page should be opened with the Title "You logged into a secure area!"

  Scenario: user should NOT be able to login using wrong password
    Given login Page is opened
    And user's email "tomsmith"
    And user's password "999990000999"
    When I click on login button
    Then The new Page should NOT be opened and message "Your password is invalid!" should be displayed.

  Scenario: user should not be able to login using wrong email
    Given login Page is opened
    And user's email "wrongEmail@gmail.com"
    And user's password "SuperSecretPassword!"
    When I click on login button
    Then The new Page should NOT be opened and message "Your username is invalid!" should be displayed.