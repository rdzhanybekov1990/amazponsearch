Feature: Dogs Store APIs

  @statusPending
  Scenario: Find By Status API - Status: Pending

    Given Status is "pending"
    When I call findByStatus API
    Then Response status should be "pending"

    Given Status is "sold"
    When I call findByStatus API
    Then Response status should be "close"

