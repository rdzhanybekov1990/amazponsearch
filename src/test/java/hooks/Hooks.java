package hooks;

import io.restassured.RestAssured;
import testDriver.TestDriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.apache.log4j.BasicConfigurator;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Hooks {

    public static WebDriver driver;
    private static Logger logger = LoggerFactory.getLogger(Hooks.class);


    @Before
    public void setUp() {
        //BasicConfigurator.configure();
      //  driver = TestDriver.getDriver();
    }

    @After
    public void teardownMethod(Scenario result) {

        TestDriver.closeDriver();
        logger.info(" ****************************************************************************************** ");
        logger.info(" **************** Finished Test Method:" + result.getName());
        logger.info(" ****************************************************************************************** ");
        logger.info(" **************** Status Of The Test:" + result.getStatus());
        logger.info(" ****************************************************************************************** ");
    }
}

