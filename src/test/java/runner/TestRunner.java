package runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import static io.cucumber.junit.CucumberOptions.SnippetType.CAMELCASE;

@RunWith(Cucumber.class)

@CucumberOptions(
        tags = "@statusPending",
        features = "src/test/resources/features/",
        glue = "",
        plugin = {"pretty",
                "junit:target/cucumber-reports/cucumber.xml",
                "json:target/cucumber-reports/cucumber.json",
                "html:target/cucumber-reports/cucumber.html"},
        snippets = CAMELCASE,

        dryRun = false
)

public class TestRunner {

}
