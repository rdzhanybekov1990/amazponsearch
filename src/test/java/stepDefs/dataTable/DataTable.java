package stepDefs.dataTable;

import io.cucumber.java.en.*;

import java.util.List;

public class DataTable {

    //Map<String,Integer> numbers;
    List<Integer> numbers;
    int result;
    int sum;

    @Given("number")
    //public void number(Map<String, Integer> numbers) {
    public void number(List<Integer> numbers) {
        this.numbers = numbers;
        System.out.println(numbers);
    }

    @When("I divide it by {int}")
    public void i_divide_it_by(Integer int1) {
        for (Integer i : numbers) {
            result = i / int1;
            sum = sum + result;
        }
    }

    @Then("I should get a sum of the results")
    public void I_should_get_a_sum_of_the_results() {
        System.out.println("Sum is: " + sum);
    }
}
