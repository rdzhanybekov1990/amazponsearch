package stepDefs.amazon;

import hooks.Hooks;
import io.cucumber.java.en.*;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import pages.ProductPage;
import pages.ShoppingCart;
import utilities.Configuration;

public class AddingProduct {

    private static Logger logger = LoggerFactory.getLogger(AddingProduct.class);

    private String expectedTitle = "Amazon.com: Samsung Galaxy A20s A207M 32GB DUOS GSM Unlocked Phone (International Variant/US Compatible LTE) - Black: Electronics";
    private String expectedProductAddedText = "Added to Cart";
    private String actualProductAddedText;
    private WebDriver driver = Hooks.driver;

    private ProductPage productPage = new ProductPage(driver);
    private ShoppingCart shoppingCart = new ShoppingCart(driver);

    @Given("I am on product page")
    public void iAmOnProductPage() {
        driver.get(Configuration.getProperty("urlProductPage"));
        String actualTitle = driver.getTitle();
        logger.info("The actual title: " + actualTitle);
        logger.info("The expected title: " + expectedTitle);
        Assert.assertEquals(actualTitle, expectedTitle);
    }

    @When("I add a product")
    public void iAddAProduct() {
        productPage.addToCartButton.click();
        actualProductAddedText = shoppingCart.addedToCartText.getText();
        logger.info("Actual product added text is: " + actualProductAddedText);

    }

    @Then("the product should be in the cart")
    public void theProductShouldBeInTheCart() {
        Assert.assertEquals(actualProductAddedText,expectedProductAddedText);
    }

}
