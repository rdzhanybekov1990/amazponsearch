package stepDefs.amazon;

import hooks.Hooks;
import io.cucumber.java.en.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import pages.ProductPage;

public class ColorSelector {

    private static Logger logger = LoggerFactory.getLogger(ColorSelector.class);

    private WebDriver driver = Hooks.driver;

    private String actualColor;

    private ProductPage productPage = new ProductPage(driver);
    private WebDriverWait wait = new WebDriverWait(driver, 10);

    @When("I change a color of product")
    public void iChangeAColorOfProduct() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(productPage.blueColorButton));
        productPage.blueColorButton.click();
        wait.until(ExpectedConditions.visibilityOf(productPage.blueColorText));
        actualColor = productPage.blueColorText.getText();
        logger.info("Actual color of chosen product is: " + actualColor);
    }

    @Then("the name of the color title should change")
    public void theNameOfTheColorTitleShouldChange() {
        String expectedColor = "Blue";
        Assert.assertEquals(actualColor, expectedColor);
    }


}
