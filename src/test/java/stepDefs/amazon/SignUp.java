package stepDefs.amazon;

import hooks.Hooks;
import io.cucumber.java.en.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.AmazonHomePage;

public class SignUp {

    private WebDriver driver = Hooks.driver;

    private AmazonHomePage amazonHomePage = new AmazonHomePage(driver);

    @When("I click on Sign in button")
    public void iClickOnSignInButton() throws InterruptedException {
        Actions actions = new Actions(driver);
        WebElement target = driver.findElement(By.xpath("(//span[@class=\"nav-line-1\"])[2]"));
        actions.moveToElement(target).perform();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[@class=\"nav-action-inner\"])[1]")));
        element.click();
    }

    @When("I click on Create your Amazon Account")
    public void iClickOnCreateYourAmazonAccount() {

    }

    @Given("Sign up page is open")
    public void signUpPageIsOpen() {

    }

    @Given("I have first password filled as {string}")
    public void iHaveFirstPasswordFilledAs(String string) {

    }

    @Given("I have confirm password filled as {string}")
    public void iHaveConfirmPasswordFilledAs(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @When("I send the form")
    public void iSendTheForm() {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

    @Then("{string} should be displayed.")
    public void shouldBeDisplayed(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new io.cucumber.java.PendingException();
    }

}
