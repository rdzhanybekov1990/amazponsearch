package stepDefs.amazon;

import hooks.Hooks;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import pages.AmazonAfterSearch;
import pages.AmazonHomePage;
import utilities.Configuration;

import java.util.List;

public class BookSearch {

    private static Logger logger = LoggerFactory.getLogger(BookSearch.class);

    private WebDriver driver = Hooks.driver;

    private String amazonSearchPageExpected = "Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & more";
    private AmazonHomePage amazonHomePage = new AmazonHomePage(driver);
    private AmazonAfterSearch amazonAfterSearch = new AmazonAfterSearch(driver);

    @Given("Amazon Web Page is Open")
    public void amazon_Web_Page_is_Open() {
        driver.get(Configuration.getProperty("url"));
        String actual = driver.getTitle();
        Assert.assertEquals(actual, amazonSearchPageExpected);
    }

    @When("I search for keyword {string}")
    public void i_search_for_keyword(String searchString) {
        amazonHomePage.clickSearchBox();
        amazonHomePage.clear();
        amazonHomePage.searchBox.sendKeys(searchString);
        amazonHomePage.submitSearch();
    }

    @Then("Result should be a new Page with title {string}")
    public void result_should_be_a_new_Page_with_title(String expectedAfterSearchTitle) {
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, expectedAfterSearchTitle);
    }

    @Then("List of {string} more than {int}")
    public void list_of_more_than(String string, Integer int1) {
        List<WebElement> listOfBooks = amazonAfterSearch.allItems;
        Assert.assertTrue(listOfBooks.size() > int1);
        logger.info("size of List: " + listOfBooks.size());
    }

    @Given("expedia page")
    public void expediaPage() {

    }

    @When("I click and fill up")
    public void iClickAndFillUp() {


    }

    @Then("All works")
    public void allWorks() {
    }

}
