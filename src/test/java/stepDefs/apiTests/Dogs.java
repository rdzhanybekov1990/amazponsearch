package stepDefs.apiTests;

import apiCalls.DogAPIs;
import io.cucumber.java.en.*;
import io.restassured.response.Response;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Dogs {

    private String status;
    private Response response;

    @Given("Status is {string}")
    public void statusIs(String status) {
        this.status = status;
    }

    @When("I call findByStatus API")
    public void iCallFindByStatusAPI() {
        response = DogAPIs.getFindByStatus(status);
    }

    @Then("Response status should be {string}")
    public void responseStatusShouldBe(String string) {
        Assert.assertEquals(response.getStatusCode(), 200);

        List<Map<String, Object>> responseBody = response.jsonPath().getList("");
        System.out.println("=============================================");
        System.out.println("The size of the LIST IS: " + responseBody.size());
        System.out.print(responseBody);

        String expectedStatus = responseBody.get(0).get("status").toString();
        System.out.println("!!!!!STATUS IS: " + expectedStatus);

    }
}
