package stepDefs.Login;

import hooks.Hooks;
import io.cucumber.java.en.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Login {

    String expectedTitle = "The Internet";
    String userEmail;
    String password;

    WebDriver driver = Hooks.driver;

    @Given("login Page is opened")
    public void login_Page_is_opened() {
        driver.get("http://the-internet.herokuapp.com/login");
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, expectedTitle);
    }

    @And("user's email {string}")
    public void user_s_email(String email) {
        userEmail = email;
        WebElement login = driver.findElement(By.id("username"));
        login.sendKeys(userEmail);
    }

    @And("user's password {string}")
    public void user_s_password(String password) {
        this.password = password;
        WebElement passw = driver.findElement(By.id("password"));
        passw.sendKeys(this.password);
    }

    @When("I click on login button")
    public void i_click_on_login_button() {
        WebElement loginButton = driver.findElement(By.xpath("//button[@class='radius']"));
        loginButton.click();
    }

    @Then("The new Page should be opened with the Title {string}")
    public void the_new_Page_should_be_opened_with_the_Title(String expectedTitle) {
        WebElement secureAreaMsg = driver.findElement(By.xpath("(//div)[3]"));
        String secureAreString = secureAreaMsg.getText();
        System.out.println(secureAreString);
        Assert.assertTrue(secureAreString.contains(expectedTitle));
    }

    @Then("The new Page should NOT be opened and message {string} should be displayed.")
    public void the_new_Page_should_NOT_be_opened_and_message_should_be_displayed(String expectedFailedText) {
        WebElement failedText = driver.findElement(By.id("flash"));
        String actualFailedText = failedText.getText();
        Assert.assertTrue(actualFailedText.contains(expectedFailedText));

    }
}
