package testDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import utilities.Configuration;

public class TestDriver {

    private static WebDriver driver;

    public static WebDriver getDriver() {
        if (driver == null) {
            switch (Configuration.getProperty("typeOfBrowser")) {
                case "chrome":
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver();
                    break;
                case "firefox":
                    WebDriverManager.firefoxdriver().setup();
                    driver = new FirefoxDriver();
                    break;
            }
            driver.manage().window().maximize();
            driver.manage().deleteAllCookies();
        }
        return driver;
    }

    public static void closeDriver() {
        if(driver !=null) {
            driver.close();
            driver.quit();
            driver = null;
        }
    }
}