package apiCalls;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DogAPIs {

    private static Logger logger = LoggerFactory.getLogger(DogAPIs.class);

    private static  String uri = "https://petstore.swagger.io/v2";

    public static Response getFindByStatus(String status) {

        RestAssured.baseURI = uri;

        RequestSpecification request = RestAssured.given().log().all();

        request.queryParam("status",status);
//        request.body("{dsjlf;jasf}");
//        request.headers();
//        request.cookies("sdf","sdf");

        Response response = request.get(uri + "/pet/findByStatus");

        return response;

    }
}
