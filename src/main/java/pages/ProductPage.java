package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductPage {

    private static Logger logger = LoggerFactory.getLogger(ProductPage.class);
    public static WebDriverWait wait;

    public ProductPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(id ="add-to-cart-button")
    public WebElement addToCartButton;

    @FindBy(id = "color_name_2")
    public WebElement blueColorButton;

    @FindBy(xpath = "(//span[@class='selection'])[1]")
    public WebElement blueColorText;

}
