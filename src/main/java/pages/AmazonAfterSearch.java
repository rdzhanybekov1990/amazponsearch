package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class AmazonAfterSearch {

    private static Logger logger = LoggerFactory.getLogger(AmazonHomePage.class);
    public static WebDriverWait wait;

    public AmazonAfterSearch(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(className = "a-size-medium")
    public List<WebElement> allItems = new ArrayList<>();
    @FindBy(className = "swatchElement")
    public List<WebElement> prices = new ArrayList<>();

}
