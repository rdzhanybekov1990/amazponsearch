package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShoppingCart {

    private static Logger logger = LoggerFactory.getLogger(ShoppingCart.class);
    public static WebDriverWait wait;

    public ShoppingCart(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(id ="huc-v2-order-row-confirm-text")
    public WebElement addedToCartText;
}
