package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AmazonHomePage {

    public AmazonHomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    private static Logger logger = LoggerFactory.getLogger(AmazonHomePage.class);

    @FindBy(id = "twotabsearchtextbox")
    public WebElement searchBox;

    @FindBy(xpath = "(//input[@type='submit'])[1]")
    public WebElement searchSubmit;

    public void clickSearchBox() {
        searchBox.click();
    }

    public void submitSearch() {
        searchSubmit.submit();
    }

    public void clear() {
        searchBox.clear();
    }

}

